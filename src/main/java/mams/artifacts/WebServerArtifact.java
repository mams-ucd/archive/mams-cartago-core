package mams.artifacts;

import mams.handlers.BaseHandler;
import mams.web.WebServer;
import cartago.*;

public class WebServerArtifact extends Artifact {
	@OPERATION
	void init(int port) {
		defineObsProperty("port", port);
        WebServer.getInstance(port);
		System.out.println("Web Server started at " + port);
	}
    
    @LINK
	public void attach(String id, BaseHandler base) {
        WebServer.getInstance().createContext(id, base);
	}
}
